#### Overview
This is an implementation of the vote counting application that helps people enter their votes on to a virtual ballot paper, then counts
the votes to select a winner. The application is based on the specification provided by the file vote-counting.pdf, 401166 
bytes, MD5:69dedb6451d19ed769625288ba2baa80.

#### Development philosophy
1.  Keep it simple.
2.  Follow the [Lean Software Development](https://en.wikipedia.org/wiki/Lean_software_development) approach. But to support this approach ensure test coverage is 
    adequate to allow for future enhancements and refactoring. 
3.  Adopt the [Fail Early/Fast principle](https://en.wikipedia.org/wiki/Fail-fast).

#### Design and approach
- This application was selected partly because it was the 1st option and on the initial read seemed relatively simple and strait forward.
- The approach was to name and design classes and methods as close as possible to the specification with the idea that someone that was familiar with the specification
  could easily read and understand the source code.
- An assumption was made that no more than 26 candidates/options will be used. This is based on the request that a sequence of unique letter prefixes (being A to Z) 
  be used as user input when casting votes. The application can be enhanced to additionally use number 0 to 9 to allow 36 candidates/options.
- To be of use, validation is done to ensure that more than one candidate/option is provided.
- Ballot papers without any votes are discarded.
- The word to indicate that the user has captured all the votes and the vote counting needs to start, "tally", may be entered by the user in mixed case.
- The letter sequence typed in by the user can also be mixed case.

#### Prerequisites for running the application
1.  Java SDK 1.8 or higher
2.  Maven 3.5.x or higher

#### How to run the application from the project folder
Before running the application, populate a file with the voting candidates/options and make that file available in the current folder as e.g. candidates.txt
  
##### Option 1 (using/expecting candidates.txt in the current folder - see the pom.xml where named file is specified as an argument to the maven exec plugin)
`mvn package exec:exec`

##### Option 2  (expecting a filename containing the voting candidates/options as a command line argument)
`mvn package` 

`java -jar target/vote-counting-1.0.0-SNAPSHOT.jar candidates.txt`

#### Notes
- It was assumed that the person "marking" the technical challenge will in all likelyhood have both the Java SDK and Maven installed.
- The tests passed and the application worked as expected on a Mac running macos 10.14.2, Java 1.8.0_162 and Maven 3.5.3
- To run the application without executing the unit tests, run `mvn -DskipTests package exec:exec`
- To view the project dependencies, run `mvn dependency:tree`

#### Class diagram (drawn in UMLet)
![Class diagram](class-diagram.bmp "Class diagram")

#### Tags
Java, Maven, Junit, [UMLet](https://en.wikipedia.org/wiki/UMLet), [Markdown](https://en.wikipedia.org/wiki/Markdown).