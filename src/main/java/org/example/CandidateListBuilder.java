package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * Utility class to build a collection of candidates/options by reading a file.
 *
 */
public final class CandidateListBuilder {

    private static final char[] A_TO_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    static final String MORE_THAN_MAXIMUM_CANDIDATES_ERROR_MESSAGE = "no more than " + A_TO_Z.length + " candidates/options are allowed";
    static final String LESS_THAN_MINIMUM_CANDIDATES_ERROR_MESSAGE = "more than 1 candidate/option is required";

    private CandidateListBuilder() {
    }

    /**
     * Read the candidates/options from a file.
     *
     * @param filenameContainingCandidateDescriptions
     * @return collection of candidates/options
     * @throws IOException
     */
    public static Set<Candidate> readFromFile(final String filenameContainingCandidateDescriptions) throws IOException {
        final Map<String, Candidate> candidates = new HashMap<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filenameContainingCandidateDescriptions))) {
            String line;
            int counter = 0;

            while (null != (line = bufferedReader.readLine())) {
                line = line.trim();

                if (!line.isEmpty() && !candidates.containsKey(line)) {

                    if (counter >= A_TO_Z.length) {
                        throw new IllegalArgumentException(MORE_THAN_MAXIMUM_CANDIDATES_ERROR_MESSAGE);
                    }
                    candidates.put(line, new Candidate(A_TO_Z[counter++], line));
                }
            }
        }
        if (!(candidates.size() > 1)) {
            throw new IllegalArgumentException(LESS_THAN_MINIMUM_CANDIDATES_ERROR_MESSAGE);
        }
        return new TreeSet<>(candidates.values());
    }
}
