package org.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * The voting ballot that contains votes for one or more votes for candidates.
 *
 */
public class Ballot {
    static final String EMPTY_VOTE_LIST_ERROR_MESSAGE = "the list of votes must not be null or empty";
    static final String NULL_CANDIDATE_ERROR_MESSAGE = "the candidate/option must not be null";

    private final List<Candidate> votesForCandidates = new ArrayList<>();

    public Ballot(final List<Candidate> votesForCandidatesInOrderOfPreference) {
        Util.assertNotEmpty(votesForCandidatesInOrderOfPreference, EMPTY_VOTE_LIST_ERROR_MESSAGE);

        for (final Candidate candidate : votesForCandidatesInOrderOfPreference) {
            Util.assertNotNull(candidate, NULL_CANDIDATE_ERROR_MESSAGE);

            if (!votesForCandidates.contains(candidate)) { // discarding votes for candidates used more than once
                votesForCandidates.add(candidate);
            }
        }
    }

    int getVotesForCandidatesCount() {
        return votesForCandidates.size();
    }

    public Candidate getFirstCandidate() {
        return votesForCandidates.get(0);
    }

    public void removeVoteForCandidate(final Candidate candidate) {
        votesForCandidates.remove(candidate);
    }

    public boolean hasMoreVotesForCandidates() {
        return !votesForCandidates.isEmpty();
    }

    public Set<Candidate> getCandidates() {
        return new HashSet<>(votesForCandidates);
    }

    @Override
    public String toString() {
        return "Ballot [votesForCandidates=" + votesForCandidates + "]";
    }
}
