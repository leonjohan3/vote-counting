package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * Counts the votes on the ballot papers to select a candidate/option as a winner.
 *
 */
public class VoteCounter {
    static final String EMPTY_BALLOT_LIST_ERROR_MESSAGE = "the list of ballots must not be null or empty";
    static final String QUOTA_MESSAGE = "The quota required to win is ";
    static final String ELIMINATE_MESSAGE = " eliminated";
    static final String ELIMINATE_RANDOM_MESSAGE = "We have a tie, selecting a random candidate for elimination from ";
    static final String NUMBER_OF_VOTES_MESSAGE = "Number of preferential votes for '";
    static final String NULL_BALLOT_ERROR_MESSAGE = "the ballot must not be null";

    private final Map<Candidate, List<Ballot>> ballots = new TreeMap<>();

    public VoteCounter(final List<Ballot> someBallots) {
        Util.assertNotEmpty(someBallots, EMPTY_BALLOT_LIST_ERROR_MESSAGE);

        // allocate ballots to candidate/options columns by 1st choice
        for (final Ballot ballot : someBallots) {
            Util.assertNotNull(ballot, NULL_BALLOT_ERROR_MESSAGE);

            final Candidate firstCandidate = ballot.getFirstCandidate();

            if (!ballots.containsKey(firstCandidate)) {
                ballots.put(firstCandidate, new ArrayList<>());
            }
            ballots.get(firstCandidate).add(ballot);
        }
    }

    /**
     * Calculate an election winner.
     *
     * @return the candidate/option that won the election.
     */
    public Candidate getWinner() {
        Candidate winner = null;
        eliminateCandidatesFromElectionThatGotNoPreferentialVotes();

        do {
            final int quota = calculateQuota(countTheNumberOfNonExhaustedBallots());
            System.out.println(QUOTA_MESSAGE + quota);
            printNumberOfVotesCurrentlyAssignedToEachCandidate();
            final Set<Candidate> winners = getCandidatesThatHaveReachedTheQuota(quota);

            if (winners.isEmpty()) {
                final Set<Candidate> losers = getCandidatesWithTheLeastNumberOfVotes();
                eliminateCandidatesFromElection(losers);
                reAssignToTheNextAvailablePreference(losers);
            } else {
                winner = winners.iterator().next();
            }
        } while (null == winner);

        return winner;
    }

    private void eliminateCandidatesFromElectionThatGotNoPreferentialVotes() {
        final Set<Candidate> candidates = new HashSet<>();
        final Set<Candidate> candidatesThatGotNoPreferentialVotes = new HashSet<>();

        for (final List<Ballot> ballotList : ballots.values()) {
            for (final Ballot ballot : ballotList) {
                candidates.addAll(ballot.getCandidates());
            }
        }

        for (final Candidate candidate : candidates) {
            if (!ballots.containsKey(candidate)) {
                candidatesThatGotNoPreferentialVotes.add(candidate);
                System.out.println("Candidate '" + candidate.getUniqueLetterPrefix() + ". " + candidate.getDescription() + "'" + ELIMINATE_MESSAGE);
            }
        }
        eliminateCandidatesFromElection(candidatesThatGotNoPreferentialVotes);
    }

    private void printNumberOfVotesCurrentlyAssignedToEachCandidate() {
        ballots.entrySet().forEach(e -> System.out.println(
                        NUMBER_OF_VOTES_MESSAGE + e.getKey().getUniqueLetterPrefix() + ". " + e.getKey().getDescription() + "' is " + e.getValue().size()));
    }

    private int calculateQuota(final int numberOfNonExhaustedBallots) {
        return numberOfNonExhaustedBallots <= 1 ? 1 : numberOfNonExhaustedBallots / 2 + 1;
    }

    private int countTheNumberOfNonExhaustedBallots() {
        int result = 0;

        for (final List<Ballot> ballotList : ballots.values()) {
            result += ballotList.size();
        }
        return result;
    }

    private void reAssignToTheNextAvailablePreference(final Set<Candidate> candidates) {
        for (final Candidate candidate : candidates) {
            final List<Ballot> ballotList = ballots.get(candidate);

            for (final Ballot ballot : ballotList) {
                if (ballot.hasMoreVotesForCandidates()) {
                    ballots.get(ballot.getFirstCandidate()).add(ballot);
                }
            }
            ballots.remove(candidate);
            System.out.println("Candidate '" + candidate.getUniqueLetterPrefix() + ". " + candidate.getDescription() + "'" + ELIMINATE_MESSAGE);
        }
    }

    private void eliminateCandidatesFromElection(final Set<Candidate> candidates) {
        for (final Candidate candidate : candidates) {
            for (final List<Ballot> ballotList : ballots.values()) {
                for (final Ballot ballot : ballotList) {
                    ballot.removeVoteForCandidate(candidate);
                }
            }
        }
    }

    private Set<Candidate> getCandidatesWithTheLeastNumberOfVotes() {
        final Set<Candidate> result = new HashSet<>();
        int minimumVoteCount = ballots.entrySet().iterator().next().getValue().size();
        int maximumVoteCount = minimumVoteCount;

        for (final Candidate candidate : ballots.keySet()) {
            final int size = ballots.get(candidate).size();
            minimumVoteCount = Math.min(minimumVoteCount, size);
            maximumVoteCount = Math.max(maximumVoteCount, size);
        }
        if (minimumVoteCount < maximumVoteCount) {

            for (final Candidate candidate : ballots.keySet()) {
                if (ballots.get(candidate).size() == minimumVoteCount) {
                    result.add(candidate);
                }
            }
        } else {
            System.out.println(ELIMINATE_RANDOM_MESSAGE + ballots.keySet());
            result.add((Candidate) Arrays.asList(ballots.keySet().toArray()).get(Util.generateRandomNumberInRange(0, ballots.keySet().size() - 1)));
        }
        return result;
    }

    private Set<Candidate> getCandidatesThatHaveReachedTheQuota(final int quota) {
        final Set<Candidate> result = new HashSet<>();

        for (final Candidate candidate : ballots.keySet()) {
            if (ballots.get(candidate).size() >= quota) {
                result.add(candidate);
            }
        }
        return result;
    }
}
