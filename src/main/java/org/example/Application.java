package org.example;

import java.io.IOException;
import java.util.Properties;

/**
 *
 * The class with the main method or entry point to the application.
 *
 */
public final class Application {

    private Application() {
    }

    /**
     * Main method or entry point of the application.
     *
     * @param args
     *            command line parameters. Needs to contain the path of the filename with the candidates/options.
     * @throws IOException
     */
    public static void main(final String[] args) throws IOException {

        if (args.length <= 0) {
            usage();
            System.exit(1);
        }

        final VoteCounter voteCounter = new VoteCounter(VoteCollector.collectVotesFromUser(CandidateListBuilder.readFromFile(args[0])));
        System.out.println("Candidate selected as winner is: " + voteCounter.getWinner().getDescription());
    }

    /**
     * Prints the usage of how to run/start the application to stderr when the user did not specify the filename containing the candidates/options.
     *
     * @throws IOException
     */
    private static void usage() throws IOException {
        System.err.println(String.format("\nError, expecting a path to the filename containing the voting candidates/options as a command line argument\n"));

        // extracting the application version (for the usage message) from the pom.properties file inside the maven built jar file as that could change over time.
        final Properties pomProperties = new Properties();
        pomProperties.load(Application.class.getClassLoader().getResourceAsStream("META-INF/maven/org.example/vote-counting/pom.properties"));
        final String applicationVersion = pomProperties.getProperty("version", "1.0.0-SNAPSHOT");

        System.err.println(String.format("Usage: java -jar target/vote-counting-%s.jar /some/path/candidates.txt\n", applicationVersion));
    }
}
