package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * Utility class to collect the votes for candidates from the user.
 *
 */
public final class VoteCollector {

    private static final String STOP_WORD = "tally";

    private VoteCollector() {
    }

    /**
     * Collect the votes from the user.
     *
     * @param candidates
     *            the list of candidates/options that the users can vote for
     *
     * @return the votes grouped by ballots that the users selected
     *
     */
    public static List<Ballot> collectVotesFromUser(final Set<Candidate> candidates) {
        final List<Ballot> ballots = new ArrayList<>();
        final Map<Character, Candidate> candidatesByUniqueLetterPrefixes = new HashMap<>();
        candidates.forEach(e -> candidatesByUniqueLetterPrefixes.put(e.getUniqueLetterPrefix(), e));
        String userInput = "";

        try (Scanner scanner = new Scanner(System.in)) {

            do {

                for (final Candidate candidate : candidates) {
                    System.out.println(candidate.getUniqueLetterPrefix() + ". " + candidate.getDescription());
                }
                System.out.print("> ");
                userInput = scanner.nextLine();

                if (!userInput.trim().isEmpty() && !userInput.trim().toLowerCase().equals(STOP_WORD)) {
                    final List<Candidate> candidatesForBallot = new ArrayList<>();

                    for (final char uniqueLetterPrefix : userInput.toUpperCase().toCharArray()) {
                        if (candidatesByUniqueLetterPrefixes.containsKey(uniqueLetterPrefix)) {
                            candidatesForBallot.add(candidatesByUniqueLetterPrefixes.get(uniqueLetterPrefix));
                        } else {
                            System.out.println("Warning: invalid candidate letter prefix '" + uniqueLetterPrefix + "' is invalid, and will be ignored");
                        }
                    }
                    if (!candidatesForBallot.isEmpty()) {
                        ballots.add(new Ballot(candidatesForBallot));
                    }
                }

            } while (!userInput.trim().toLowerCase().equals(STOP_WORD));
        }
        return ballots;
    }
}
