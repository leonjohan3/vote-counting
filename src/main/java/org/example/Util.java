package org.example;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * Reusable Utilities. The Spring Framework will normally provide these in a real life application.
 *
 */
public final class Util {

    private Util() {
    }

    /**
     * Assert that the given String matches the provided regex.
     *
     * @param text
     *            the String to check
     * @param regex
     *            the regex that the string should match
     * @param message
     *            the exception message to use if the assertion fails
     *
     * @throws IllegalArgumentException
     */
    public static void assertMatches(final String text, final String regex, final String message) {
        if (!text.matches(regex)) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Assert that the given String has valid text content; that is, it must not be <code>null</code> and must contain at least one non-whitespace character.
     *
     * <pre class="code">
     * assertHasText(name, "'name' must not be empty");
     * </pre>
     *
     * @param text
     *            the String to check
     * @param message
     *            the exception message to use if the assertion fails
     *
     * @throws IllegalArgumentException
     *
     * @see #hasText
     */
    public static void assertHasText(final String text, final String message) {
        if (!hasText(text)) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Check that the given CharSequence is neither <code>null</code> nor of length 0. Will return <code>true</code> for a CharSequence that purely consists of
     * whitespace.
     *
     * <pre>
     * Util.hasLength(null) = false
     * Util.hasLength("") = false
     * Util.hasLength(" ") = true
     * Util.hasLength("Hello") = true
     * </pre>
     *
     * @param str
     *            the CharSequence to check (may be <code>null</code>)
     *
     * @return <code>true</code> if the CharSequence is not null and has length
     *
     * @see #hasText(String)
     */
    public static boolean hasLength(final CharSequence str) {
        return null != str && str.length() > 0;
    }

    /**
     * Check whether the given CharSequence has text. More specifically, returns <code>true</code> if the string not <code>null</code>, its length is greater than
     * 0, and it contains at least one non-whitespace character.
     *
     * <pre>
     * Util.hasText(null) = false
     * Util.hasText("") = false
     * Util.hasText(" ") = false
     * Util.hasText("12345") = true
     * Util.hasText(" 12345 ") = true
     * </pre>
     *
     * @param str
     *            the CharSequence to check (may be <code>null</code>)
     *
     * @return <code>true</code> if the CharSequence is not <code>null</code>, its length is greater than 0, and it does not contain whitespace only
     */
    public static boolean hasText(final CharSequence str) {

        if (!hasLength(str)) {
            return false;
        }

        for (int i = 0; i < str.length(); i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Assert that the collection is not <code>null</code> and its length is greater than 0.
     *
     * @param collection
     *            the collection to check (may be <code>null</code>)
     * @param message
     *            the exception message to use if the assertion fails
     *
     * @throws IllegalArgumentException
     */
    public static void assertNotEmpty(final Collection<?> collection, final String message) {
        if (null == collection || collection.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Assert that the object is not null.
     *
     * @param object
     *            the object to check
     * @param message
     *            the exception message to use if the assertion fails
     *
     * @throws IllegalArgumentException
     */
    public static void assertNotNull(final Object object, final String message) {
        if (Objects.isNull(object)) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Generate a random number within a specific range
     *
     * @param minimum
     *            the smallest random number to generate
     * @param maximum
     *            the largest random number to generate
     *
     * @return a random number between minimum and maximum inclusive
     */
    public static int generateRandomNumberInRange(final int minimum, final int maximum) {
        return ThreadLocalRandom.current().nextInt(minimum, maximum + 1);
    }
}
