package org.example;

/**
 *
 * Candidate/option
 *
 */
public final class Candidate implements Comparable<Candidate> {

    static final String INVALID_UNIQUE_LETTER_PREFIX_ERROR_MESSAGE = "the only valid values for uniqueLetterPrefix is a single letter from A to Z";
    static final String BLANK_DESCRIPTION_ERROR_MESSAGE = "the description must not be null, empty or blank";

    private final char uniqueLetterPrefix;
    private final String description;

    public Candidate(final char aUniqueLetterPrefix, final String aDescription) {
        Util.assertMatches(new Character(aUniqueLetterPrefix).toString(), "[A-Z]", INVALID_UNIQUE_LETTER_PREFIX_ERROR_MESSAGE);
        Util.assertHasText(aDescription, BLANK_DESCRIPTION_ERROR_MESSAGE);
        uniqueLetterPrefix = aUniqueLetterPrefix;
        description = aDescription;
    }

    public char getUniqueLetterPrefix() {
        return uniqueLetterPrefix;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Candidate [uniqueLetterPrefix=" + uniqueLetterPrefix + ", description=" + description + "]";
    }

    @Override
    public int hashCode() {
        return new String(new Character(uniqueLetterPrefix).toString() + description).hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final Candidate other = (Candidate) obj;

        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!(new Character(uniqueLetterPrefix).toString() + description).equals(new Character(other.uniqueLetterPrefix).toString() + other.description)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(final Candidate o) {
        return new String(new Character(uniqueLetterPrefix).toString() + description).compareTo(new Character(o.uniqueLetterPrefix).toString() + o.description);
    }
}
