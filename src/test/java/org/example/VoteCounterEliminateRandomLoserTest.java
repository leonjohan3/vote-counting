package org.example;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * VoteCounter Test for eliminating a random loser.
 *
 */
public class VoteCounterEliminateRandomLoserTest {

    private final ByteArrayOutputStream stdout = new ByteArrayOutputStream();

    @Before
    public void doThisBeforeEachTestMethod() {
        System.setOut(new PrintStream(stdout));
    }

    @After
    public void doThisAfterEachTestMethod() {
        System.setOut(System.out);
    }

    @Test(timeout = 2000)
    public void shouldEliminateRandomLoser() {

        // given: all data (test fixture) preparation
        final Candidate wineryTour = new Candidate('A', "Winery tour");
        final Candidate tenPinBowling = new Candidate('B', "Ten pin bowling");
        final List<Ballot> ballots = new ArrayList<>();
        ballots.add(new Ballot(Arrays.asList(wineryTour, tenPinBowling)));
        ballots.add(new Ballot(Arrays.asList(tenPinBowling, wineryTour)));
        final VoteCounter voteCounter = new VoteCounter(ballots);

        // when : method to be checked invocation
        final Candidate winner = voteCounter.getWinner();

        // then : checks and assertions
        assertThat(winner, either(is(tenPinBowling)).or(is(wineryTour)));
        assertThat(stdout.toString(), containsString(VoteCounter.ELIMINATE_RANDOM_MESSAGE));
    }
}