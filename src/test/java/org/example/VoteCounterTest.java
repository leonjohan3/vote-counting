package org.example;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * VoteCounter Tests using the candidates/options from the vote-counting.pdf
 *
 */
public class VoteCounterTest extends ParameterizedBase {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            { new String[] { "ABDC", "DA", "BAD", "DB", "CABD", "BAC", "CDAB", "CBAD" }, 'B' },
            { new String[] { "ACBD", "DA", "BAD", "DB", "CABD", "BAC", "CDAB", "CBAD" }, 'C' },
            { new String[] { "BAD", "BAC", "CABD", "CDAB", "CBAD", "DA", "DB" }, 'C' },
            { new String[] { "ABDC", "BAD", "BAC", "CABD", "CDAB", "CDA", "DA", "DB" }, 'B' },
            { new String[] { "ADB", "BDA", "BA", "CDAB", "CDAB", "CDBA", "DA", "DB" }, 'D' },
            { new String[] { "D" }, 'D' },
            { new String[] { "D", "B", "DB" }, 'D' },
            { new String[] { "D", "B", "DB", "BD", "AD" }, 'D' },
        });
    }

    @BeforeClass
    public static void doThisBeforeAllTests() throws IOException {
        for (final Candidate candidate : CandidateListBuilder.readFromFile("src/test/resources/org/example/ExampleListOfCandidatesFromPDF.txt")) {
            CANDIDATE_MAP_BY_UNIQUE_LETTER_PREFIX.put(candidate.getUniqueLetterPrefix(), candidate);
        }
    }
}