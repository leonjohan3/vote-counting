package org.example;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * VoteCounter Tests with 2 candidates/options
 *
 */
public class VoteCounterBottomEdgeTest extends ParameterizedBase {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new String[] { "AB", "A" }, 'A' },
                { new String[] { "AB", "BA", "B", "A", "BA" }, 'B' },
                { new String[] { "AB" }, 'A' },
                { new String[] { "A" }, 'A' },
            });
    }

    @BeforeClass
    public static void doThisBeforeAllTests() throws IOException {
        for (final Candidate candidate : CandidateListBuilder.readFromFile("src/test/resources/org/example/JustTwoCandidates.txt")) {
            CANDIDATE_MAP_BY_UNIQUE_LETTER_PREFIX.put(candidate.getUniqueLetterPrefix(), candidate);
        }
    }
}