package org.example;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * VoteCounter Tests, also asserting that the required output is printed.
 *
 */
public class VoteCounterSimpleTest {

    private final ExpectedException expectedException = ExpectedException.none();
    private final ByteArrayOutputStream stdout = new ByteArrayOutputStream();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Before
    public void doThisBeforeEachTestMethod() {
        System.setOut(new PrintStream(stdout));
    }

    @After
    public void doThisAfterEachTestMethod() {
        System.setOut(System.out);
    }

    @Test
    public void shouldEnsureBallotOnListNonNull() {

        // given: all data (test fixture) preparation
        final List<Ballot> ballotList = new ArrayList<>();
        ballotList.add(null);

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(VoteCounter.NULL_BALLOT_ERROR_MESSAGE));

        // when : method to be checked invocation
        new VoteCounter(ballotList);
    }

    @Test
    public void shouldEnsureBallotListNonNull() {

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(VoteCounter.EMPTY_BALLOT_LIST_ERROR_MESSAGE));

        // when : method to be checked invocation
        new VoteCounter(null);
    }

    @Test
    public void shouldEnsureBallotListIsNotEmpty() {

        // given: all data (test fixture) preparation
        final List<Ballot> emptyBallotsList = new ArrayList<>();

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(VoteCounter.EMPTY_BALLOT_LIST_ERROR_MESSAGE));

        // when : method to be checked invocation
        new VoteCounter(emptyBallotsList);
    }

    @Test(timeout = 2000)
    public void shouldSelectCorrectWinner() {

        // given: all data (test fixture) preparation
        final Candidate wineryTour = new Candidate('A', "Winery tour");
        final Candidate tenPinBowling = new Candidate('B', "Ten pin bowling");
        final Candidate movieNight = new Candidate('C', "Movie night");
        final Candidate museumVisit = new Candidate('D', "Museum visit");
        final List<Ballot> ballots = new ArrayList<>();
        ballots.add(new Ballot(Arrays.asList(wineryTour, tenPinBowling, museumVisit, movieNight)));
        ballots.add(new Ballot(Arrays.asList(museumVisit, wineryTour)));
        ballots.add(new Ballot(Arrays.asList(tenPinBowling, wineryTour, museumVisit)));
        ballots.add(new Ballot(Arrays.asList(museumVisit, tenPinBowling)));
        ballots.add(new Ballot(Arrays.asList(movieNight, wineryTour, tenPinBowling, museumVisit)));
        ballots.add(new Ballot(Arrays.asList(tenPinBowling, wineryTour, movieNight)));
        ballots.add(new Ballot(Arrays.asList(movieNight, museumVisit, wineryTour, tenPinBowling)));
        ballots.add(new Ballot(Arrays.asList(movieNight, tenPinBowling, wineryTour, museumVisit)));
        final VoteCounter voteCounter = new VoteCounter(ballots);

        // when : method to be checked invocation
        final Candidate winner = voteCounter.getWinner();

        // then : checks and assertions
        assertThat(winner, is(tenPinBowling));
        assertThat(stdout.toString(), containsString(VoteCounter.QUOTA_MESSAGE));
        assertThat(stdout.toString(), containsString(VoteCounter.ELIMINATE_MESSAGE));
        assertThat(stdout.toString(), containsString(VoteCounter.NUMBER_OF_VOTES_MESSAGE));
    }
}