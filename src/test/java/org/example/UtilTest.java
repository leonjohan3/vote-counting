package org.example;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * Util Tests
 *
 */
public class UtilTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldAssertThatStringMatchesRegex() {

        // given: all data (test fixture) preparation
        final String message = "we have an error";
        final String regex = "\\d{3}";

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(message));

        // when : method to be checked invocation
        Util.assertMatches("123", regex, message + ", not!");
        Util.assertMatches("abc", regex, message);
    }

    @Test
    public void shouldAssertThatStringHasText() {

        // given: all data (test fixture) preparation
        final String message = "we have an error";

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(message));

        // when : method to be checked invocation
        Util.assertHasText(" abc ", message + ", not!");
        Util.assertHasText(" ", message);
    }

    @Test
    public void shouldMakeSureStringHasText() {
        assertThat(Util.hasText(null), is(false));
        assertThat(Util.hasText(""), is(false));
        assertThat(Util.hasText(" "), is(false));
        assertThat(Util.hasText("  "), is(false));
        assertThat(Util.hasText("  a"), is(true));
        assertThat(Util.hasText("a  b"), is(true));
        assertThat(Util.hasText("c  "), is(true));
        assertThat(Util.hasText("abc"), is(true));
    }

    @Test
    public void shouldMakeSureStringHasLength() {
        assertThat(Util.hasLength(null), is(false));
        assertThat(Util.hasLength(""), is(false));
        assertThat(Util.hasLength(" "), is(true));
        assertThat(Util.hasLength("abc"), is(true));
    }

    @Test
    public void shouldGenerateRandomNumbersBetweenZeroAnd9() {
        final Map<Integer, Integer> randomNumbers = new HashMap<>();
        final int numberOfRandomNumbersToGenerate = 1000;
        final int minimumRandomNumber = 0;
        final int maximumRandomNumber = 9;

        for (int i = 0; i < numberOfRandomNumbersToGenerate; i++) {
            final int randomNumber = Util.generateRandomNumberInRange(minimumRandomNumber, maximumRandomNumber);
            assertThat(randomNumber, both(greaterThanOrEqualTo(minimumRandomNumber)).and(lessThanOrEqualTo(maximumRandomNumber)));

            if (!randomNumbers.containsKey(randomNumber)) {
                randomNumbers.put(randomNumber, 1);
            } else {
                randomNumbers.put(randomNumber, randomNumbers.get(randomNumber) + 1);
            }
        }
        // ensure that all numbers 0 to 9, were returned by the random number generator
        assertThat(randomNumbers.entrySet(), hasSize(maximumRandomNumber + 1));
        // ensure that the frequency distribution of the generated random numbers is reasonable
        randomNumbers.entrySet().forEach(e -> assertThat(e.getValue(), both(greaterThanOrEqualTo(100 - 40)).and(lessThanOrEqualTo(100 + 40))));
    }
}
