package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

/**
 *
 * VoteCollector Tests
 *
 */
public class VoteCollectorTest {

    @Test
    public void shouldCollectVotesFromUserIgnoringWhitespaceDuplicateInvalidCandidatesAndEmptyCandidateLists() throws IOException {

        // given: all data (test fixture) preparation
        final String simulatedUserInput = "DA D\r\nY\r\ncW\r\ntAlLy\r\n";
        List<Ballot> ballots;

        // when : method to be checked invocation
        try {
            System.setIn(new ByteArrayInputStream(simulatedUserInput.getBytes()));
            ballots = VoteCollector.collectVotesFromUser(CandidateListBuilder.readFromFile("src/test/resources/org/example/ExampleListOfCandidatesFromPDF.txt"));
        } finally {
            System.setIn(System.in);
        }

        // then : checks and assertions
        assertThat(ballots, hasSize(2));
        assertThat(ballots.get(0).getVotesForCandidatesCount(), is(2));
        assertThat(ballots.get(1).getVotesForCandidatesCount(), is(1));
        assertThat(ballots.get(0).getFirstCandidate().getUniqueLetterPrefix(), is('D'));
        assertThat(ballots.get(1).getFirstCandidate().getUniqueLetterPrefix(), is('C'));
    }
}
