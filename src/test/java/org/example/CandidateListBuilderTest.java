package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Set;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
*
* CandidateListBuilder Tests
*
*/
public class CandidateListBuilderTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldProvideMoreThanOneCandidates() throws IOException {

        // given: all data (test fixture) preparation
        final String fileName = "src/test/resources/org/example/JustaSingleCandidate.txt";

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(CandidateListBuilder.LESS_THAN_MINIMUM_CANDIDATES_ERROR_MESSAGE));

        // when : method to be checked invocation
        CandidateListBuilder.readFromFile(fileName);
    }

    @Test
    public void shouldNotAllowMoreThanMaximumNumberOfCandidates() throws IOException {

        // given: all data (test fixture) preparation
        final String fileName = "src/test/resources/org/example/MoreCandidatesThanAllowed.txt";

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(CandidateListBuilder.MORE_THAN_MAXIMUM_CANDIDATES_ERROR_MESSAGE));

        // when : method to be checked invocation
        CandidateListBuilder.readFromFile(fileName);
    }

    @Test
    public void shouldReadListOfCandidatesIgnoringWhitespaceAndDuplicates() throws IOException {

        // given: all data (test fixture) preparation
        final String fileName = "src/test/resources/org/example/LongListOfOptionsOrCandidates.txt";

        // when : method to be checked invocation
        final Set<Candidate> candidates = CandidateListBuilder.readFromFile(fileName);

        // then : checks and assertions
        assertThat(candidates, containsInAnyOrder(new Candidate('A', "Winery tour"),
                                                  new Candidate('B', "Ten pin bowling"),
                                                  new Candidate('C', "Movie night"),
                                                  new Candidate('D', "Dinner at a restaurant"),
                                                  new Candidate('E', "Art gallery visit"),
                                                  new Candidate('F', "Picnic in the park"),
                                                  new Candidate('G', "Horse riding lessons"),
                                                  new Candidate('H', "Museum visit"),
                                                  new Candidate('I', "Surfing lesson")));

    }

}
