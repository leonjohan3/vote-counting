package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * Ballot Tests
 *
 */
public class BallotTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldEnsureCandidateOnListNonNull() {

        // given: all data (test fixture) preparation
        final List<Candidate> votes = new ArrayList<>();
        votes.add(null);

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(Ballot.NULL_CANDIDATE_ERROR_MESSAGE));

        // when : method to be checked invocation
        new Ballot(votes);
    }

    @Test
    public void shouldEnsureCandidateListNonNull() {

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(Ballot.EMPTY_VOTE_LIST_ERROR_MESSAGE));

        // when : method to be checked invocation
        new Ballot(null);
    }

    @Test
    public void shouldEnsureAtLeastOneCandidateIsPresent() {

        // given: all data (test fixture) preparation
        final List<Candidate> votes = new ArrayList<>();

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(Ballot.EMPTY_VOTE_LIST_ERROR_MESSAGE));

        // when : method to be checked invocation
        new Ballot(votes);
    }

    @Test
    public void shouldNotAddDuplicateCandidates() {

        // given: all data (test fixture) preparation
        final Candidate candidateA = new Candidate('A', "abc");

        // when : method to be checked invocation
        final Ballot ballot = new Ballot(Arrays.asList(candidateA, candidateA));

        // then : checks and assertions
        assertThat(ballot.getVotesForCandidatesCount(), is(1));
    }

    @Test
    public void shouldAddNonDuplicateCandidates() {

        // given: all data (test fixture) preparation
        final Candidate candidateA = new Candidate('A', "abc");
        final Candidate candidateB = new Candidate('B', "def");

        // when : method to be checked invocation
        final Ballot ballot = new Ballot(Arrays.asList(candidateA, candidateB));

        // then : checks and assertions
        assertThat(ballot.getVotesForCandidatesCount(), is(2));
    }
}
