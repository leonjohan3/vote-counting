package org.example;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

/**
 *
 * Base class for VoteCounter* Tests
 *
 */
@RunWith(Parameterized.class)
public abstract class ParameterizedBase {

    @Parameter(0)
    public String[] ballotVotes;

    @Parameter(1)
    public char expectedWinner;

    protected static final Map<Character, Candidate> CANDIDATE_MAP_BY_UNIQUE_LETTER_PREFIX = new HashMap<>();
    private final List<Ballot> ballots = new ArrayList<>();

    @Before
    public void doThisBeforeEachTestMethod() {

        for (final String votes : ballotVotes) {
            final List<Candidate> candidateList = new ArrayList<>();

            for (final char uniqueLetterPrefix : votes.toCharArray()) {
                candidateList.add(CANDIDATE_MAP_BY_UNIQUE_LETTER_PREFIX.get(uniqueLetterPrefix));
            }
            ballots.add(new Ballot(candidateList));
        }
    }

    @Test(timeout = 2000) // all tests with the timeout is to ensure that not going into infinite loop without any winner being selected
    public void shouldSelectCorrectWinner() {

        // given: all data (test fixture) preparation
        final VoteCounter voteCounter = new VoteCounter(ballots);

        // when : method to be checked invocation
        final Candidate winner = voteCounter.getWinner();

        // then : checks and assertions
        assertThat(winner, is(CANDIDATE_MAP_BY_UNIQUE_LETTER_PREFIX.get(expectedWinner)));
    }
}
