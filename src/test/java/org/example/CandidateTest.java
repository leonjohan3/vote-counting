package org.example;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * Candidate Tests
 *
 */
public class CandidateTest {

    private final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public ExpectedException getExpectedExceptionRule() {
        return expectedException;
    }

    @Test
    public void shouldValidate1stConstructorParameter() {

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(Candidate.INVALID_UNIQUE_LETTER_PREFIX_ERROR_MESSAGE));

        // when : method to be checked invocation
        new Candidate('~', "description");
    }

    @Test
    public void shouldValidate2ndConstructorParameter() {

        // then : checks and assertions
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(is(Candidate.BLANK_DESCRIPTION_ERROR_MESSAGE));

        // when : method to be checked invocation
        new Candidate('F', " ");
    }
}